#pragma once

#include "HashFunction.h"
#include <iostream>
#include <vector>
#include <random>
#include <stdlib.h>
class MachineLearningBot{
private:
	static const int tablesizes = 6;
	std::vector<long long> unquieMatrix;
	ZobKeyBW zobristKeys[tablesizes][tablesizes];
	Hashing hashData;
	double plusWin = 0.05f;
	double minusLose = 0.2f;
	int player = -1;
public:
	MachineLearningBot(int player);
	RowCol machineLearningBotMove(int gameMatrix[tablesizes][tablesizes], std::vector<RowCol> validMove);
	RowCol findBestWinChance(int gameMatrix[tablesizes][tablesizes], std::vector<RowCol> validMove);
	void upDateBot(bool win);
	long long getUnquieNumber(int gameMatrix[tablesizes][tablesizes]);
	void printHashTable();

private:

};
