#include "Game.h"

Game::Game() {

	initGame();
}

void Game::initGame() {
	resetGame();
	whitePlayerWin = 0;
	blackPlayerWin = 0;
	bot1 = greedyBot(1);
	//bot2 = greedyBot(2);
	testBot = MachineLearningBot(2);
	
}

void Game::resetGame() {

	for (int i = 0; i < tablesizes; i++) {
		for (int j = 0; j < tablesizes; j++) {
			gameMatrix[i][j] = 0;
		}
	}
	if (tablesizes == 8) {
		gameMatrix[3][3] = 1;
		gameMatrix[3][4] = 2;
		gameMatrix[4][3] = 2;
		gameMatrix[4][4] = 1;

	}
	else if (tablesizes == 6) {
		gameMatrix[2][2] = 1;
		gameMatrix[2][3] = 2;
		gameMatrix[3][2] = 2;
		gameMatrix[3][3] = 1;
	}
	else if (tablesizes == 4) {
		gameMatrix[1][1] = 1;
		gameMatrix[1][2] = 2;
		gameMatrix[2][1] = 2;
		gameMatrix[2][2] = 1;
	}
	blackPlayerPointsCount = 0;
	whitePlayerPointsCount = 0;
	blackPlayerNoMove = false;
	whitePlayerNoMove = false;

	currentPlayer = rand() % 2 + 1;
}

void Game::printTable() {
	cout << "------------------------" << endl;
	for (int row = tablesizes - 1; row >= 0; row--) {
		for (int col = 0; col < tablesizes; col++) {
			cout << "|" << gameMatrix[row][col] << "|";
		}
		cout << endl;
	}
}

void Game::gameLoop() {
	int play = NULL;
	while (play == 0) {
		cout << "Current Score:" << endl;
		cout << "Black Player:" << blackPlayerWin << endl;
		cout << "White Player:" << whitePlayerWin << endl;
		cout << "Start Game(0) : End Game(1):";
		cin >> play;
		if (play == 0) {
			resetGame();
			bool gameplay = true;
			int turns = 0;
			while (gameplay == true) {
				/*cout << "===========================" << endl;
				cout << "Previous Turn" << endl;
				printTable();*/
				cout << "---------------------------" << endl;
				cout << "Curent Turn" << endl;
				cout << "Current Player: ";
				if (currentPlayer == 1) {
					cout << "Black" << endl;
				}
				else {
					cout << "White" << endl;
				}
				cout << "How Many Turns:" << turns << endl;
					turns++;
				if (blackPlayerNoMove == true && whitePlayerNoMove == true) {
					blackPlayerPointsCount = findPoints(1);
					whitePlayerPointsCount = findPoints(2);
					if (blackPlayerPointsCount > whitePlayerPointsCount) {
						blackPlayerWin++;
						cout << "Black Player Has Won With " << blackPlayerPointsCount << " Points" << endl;
						cout << "White Player Has " << whitePlayerPointsCount << " Points" << endl;
						testBot.upDateBot(false);
					}
					else {
						whitePlayerWin++;
						cout << "White Player Has Won With " << whitePlayerPointsCount << " Points" << endl;
						cout << "Black Player Has " << blackPlayerPointsCount << " Points" << endl;
						testBot.upDateBot(true);
					}
					gameplay = false;

				}
				if (gameplay == true) {
					vector<RowCol> validMove = findValidMove();
					if (validMove.size() == 0) {
						if (currentPlayer == 1) {
							currentPlayer = 2;
							blackPlayerNoMove = true;
							cout << "Black Player Cannot Move" << endl;
							cout << "---------------------------" << endl;
						}
						else if (currentPlayer == 2) {
							currentPlayer = 1;
							whitePlayerNoMove = true;
							cout << "White Player Cannot Move" << endl;
							cout << "---------------------------" << endl;
						}
					}
					else {
						if (currentPlayer == 1) {
							RowCol bot1play = bot1.botGreedyMove(gameMatrix, validMove);
							cout << "Bot Move: Row:" << bot1play.Row << " Col:" << bot1play.Col << endl;
							playerInput(bot1play.Row, bot1play.Col);
							blackPlayerNoMove = false ;
							currentPlayer = 2;
						}
						else {
							RowCol bot2play = testBot.machineLearningBotMove(gameMatrix, validMove);
							cout << "Bot Move: Row:" << bot2play.Row << " Col:" << bot2play.Col << endl;
							playerInput(bot2play.Row, bot2play.Col);
							whitePlayerNoMove = false;
							currentPlayer = 1;
						}
					}
				}
				printTable();
			}
			testBot.printHashTable();
		}
	}

}

//Not Needed Cause no Player
void Game::playerInput(int Row, int Col) {
	/*cout << gameMatrix[Row + 1][Col + 1] << endl;*/
	if (checkIfCanMove(Row, Col)) {
		cout << "Valid Move" << endl;
		gameMatrix[Row][Col] = currentPlayer;
		flipCheck(Row, Col);
	}
	else {
		cout << "Invalid Move" << endl;
	}
}

//Where they is a valid input for each player
bool Game::checkIfCanMove(int Row, int Col)
{
	//Check if input already have something inside
	if (gameMatrix[Row][Col] == 0) {
		/*cout << "inside" << endl;*/
		int oppsitePlayer;
		if (currentPlayer == 1) {
			oppsitePlayer = 2;
		}
		else {
			oppsitePlayer = 1;
		}
		bool isValid = false;
		//Check if valid Top
		if (gameMatrix[Row + 1][Col] == oppsitePlayer) {
			/*cout << "top" << endl;*/
			for (int temp_Row = Row + 2; temp_Row < tablesizes; temp_Row++) {
				if (gameMatrix[temp_Row][Col] == currentPlayer) {
					isValid = true;
				}
			}

		}
		//Check if valid Bottom
		else if (gameMatrix[Row - 1][Col] == oppsitePlayer) {
			/*cout << "bottom" << endl;*/
			for (int temp_Row = Row - 2; temp_Row >= 0; temp_Row--) {
				if (gameMatrix[temp_Row][Col] == currentPlayer) {
					isValid = true;
					break;
				}
			}
		}

		//Check if valid Right
		else if (gameMatrix[Row][Col + 1] == oppsitePlayer) {
			/*cout << "Right" << endl;*/
			for (int temp_Col = Col + 2; temp_Col < tablesizes; temp_Col++) {
				if (gameMatrix[Row][temp_Col] == currentPlayer) {
					isValid = true;
				}
			}
		}

		//Check if valid Left
		else if (gameMatrix[Row][Col - 1] == oppsitePlayer) {
			/*cout << "Left" << endl;*/
			for (int temp_Col = Col - 2; temp_Col >= 0; temp_Col--) {
				if (gameMatrix[Row][temp_Col] == currentPlayer) {
					isValid = true;
				}
			}
		}

		//Check if valid UpRight
		else if (gameMatrix[Row + 1][Col + 1] == oppsitePlayer) {
			/*cout << "Up_Right" << endl;*/
			int highest_Temp;
			if (Row > Col) {
				highest_Temp = Row;
			}
			else {
				highest_Temp = Col;
			}
			int temp_Row = Row + 2;
			int temp_Col = Col + 2;
			for (int i = highest_Temp + 2; i < tablesizes; i++) {
				if (gameMatrix[temp_Row][temp_Col] == currentPlayer) {
					isValid = true;
					break;
				}
				temp_Row++;
				temp_Col++;
			}
		}
		//Check if valid UpLeft
		else if (gameMatrix[Row + 1][Col - 1] == oppsitePlayer) {
			/*cout << "Up Left" << endl;*/
			int highest_Temp;
			if (abs(Col - 7) > Row) {
				highest_Temp = abs(Col - 7);
			}
			else {
				highest_Temp = Row;
			}
			int temp_Row = Row + 2;
			int temp_Col = Col - 2;
			/*cout << abs(Col - tablesizes) << ":" << Row << endl;*/
			for (int i = highest_Temp + 2; i < tablesizes; i++) {
				/*cout << temp_Row << ":" << temp_Col << endl;
				cout << gameMatrix[temp_Row][temp_Col] << endl;*/
				if (gameMatrix[temp_Row][temp_Col] == currentPlayer) {
					isValid = true;
					break;
				}
				temp_Row++;
				temp_Col--;
			}
		}

		//Check if valid BottomRight
		else if (gameMatrix[Row - 1][Col + 1] == oppsitePlayer) {
			/*cout << "Bottom Right" << endl;*/
			int highest_Temp;
			if (abs(Row - 7) > Col) {
				highest_Temp = abs(Row - 7);
			}
			else {
				highest_Temp = Col;
			}
			int temp_Row = Row - 2;
			int temp_Col = Col + 2;
			/*cout << abs(Col - tablesizes) << ":" << Row << endl;*/
			for (int i = highest_Temp + 2; i < tablesizes; i++) {
				/*cout << temp_Row << ":" << temp_Col << endl;
				cout << gameMatrix[temp_Row][temp_Col] << endl;*/
				if (gameMatrix[temp_Row][temp_Col] == currentPlayer) {
					isValid = true;
					break;
				}
				temp_Row--;
				temp_Col++;
			}
		}

		//Check if valid BottomLeft
		else if (gameMatrix[Row - 1][Col - 1] == oppsitePlayer) {
			/*cout << "Bottom Left" << endl;*/
			int highest_Temp;
			if (abs(Row - 7) > abs(Col - 7)) {
				highest_Temp = abs(Row - 7);
			}
			else {
				highest_Temp = abs(Col - 7);
			}
			int temp_Row = Row - 2;
			int temp_Col = Col - 2;
			/*cout << abs(Col - tablesizes) << ":" << Row << endl;*/
			for (int i = highest_Temp + 2; i < tablesizes; i++) {
				//cout << temp_Row << ":" << temp_Col << endl;
				//cout << gameMatrix[temp_Row][temp_Col] << endl;
				if (gameMatrix[temp_Row][temp_Col] == currentPlayer) {
					isValid = true;
					break;
				}
				temp_Row--;
				temp_Col--;
			}
		}

		if (isValid) {
			return true;
		}

	}
	return false;
}

void Game::flipCheck(int Row, int Col) {
	int oppsitePlayer;
	if (currentPlayer == 1) {
		oppsitePlayer = 2;
	}
	else {
		oppsitePlayer = 1;
	}
	vector<RowCol> storage;
	bool validflip = true;

	if (gameMatrix[Row + 1][Col] == oppsitePlayer) {
		/*cout << "check flip top" << endl;*/
		for (int temp_Row = Row + 1; temp_Row < tablesizes; temp_Row++) {
			if (gameMatrix[temp_Row][Col] == currentPlayer) {
				validflip = true;
				break;
			}
			else if (gameMatrix[temp_Row][Col] == oppsitePlayer) {
				storage.push_back(RowCol(temp_Row, Col));
			}
			else {
				validflip = false;
				break;
			}
		}
		if (validflip) {
			/*cout << "Flip Top" << endl;*/
			for (int i = 0; i < storage.size(); i++) {
				RowCol temp = storage.at(i);
				gameMatrix[temp.Row][temp.Col] = currentPlayer;
			}
		}
	}
	storage.clear();
	validflip = true;
	//check if valid bottom
	if (gameMatrix[Row - 1][Col] == oppsitePlayer) {
		/*cout << "check flip bottom" << endl;*/
		for (int temp_Row = Row - 1; temp_Row >= 0; temp_Row--) {
			if (gameMatrix[temp_Row][Col] == currentPlayer) {
				validflip = true;
				break;
			}
			else if (gameMatrix[temp_Row][Col] == oppsitePlayer) {
				storage.push_back(RowCol(temp_Row, Col));
			}
			else {
				validflip = false;
				break;
			}
		}
		if (validflip) {
			/*cout << "Flip Bot" << endl;*/
			for (int i = 0; i < storage.size(); i++) {
				RowCol temp = storage.at(i);
				gameMatrix[temp.Row][temp.Col] = currentPlayer;
			}
		}
	}

	storage.clear();
	validflip = true;

	//Check if valid Right
	if (gameMatrix[Row][Col + 1] == oppsitePlayer) {
		/*cout << "Check Flip Right" << endl;*/
		for (int temp_Col = Col + 1; temp_Col < tablesizes; temp_Col++) {
			if (gameMatrix[Row][temp_Col] == currentPlayer) {
				validflip = true;
				break;
			}
			else if (gameMatrix[Row][temp_Col] == oppsitePlayer) {
				storage.push_back(RowCol(Row, temp_Col));
			}
			else {
				validflip = false;
				break;
			}
		}
		if (validflip) {
			/*cout << "Flip Right" << endl;*/
			for (int i = 0; i < storage.size(); i++) {
				RowCol temp = storage.at(i);
				gameMatrix[temp.Row][temp.Col] = currentPlayer;
			}
		}
	}

	storage.clear();
	validflip = true;

	//Check if valid Left
	if (gameMatrix[Row][Col - 1] == oppsitePlayer) {
		/*cout << "Check Flip Left" << endl;*/
		for (int temp_Col = Col - 1; temp_Col >= 0; temp_Col--) {
			if (gameMatrix[Row][temp_Col] == currentPlayer) {
				validflip = true;
				break;
			}
			else if (gameMatrix[Row][temp_Col] == oppsitePlayer) {
				storage.push_back(RowCol(Row, temp_Col));
			}
			else {
				validflip = false;
				break;
			}
		}
		if (validflip) {
			/*cout << "Flip Left" << endl;*/
			for (int i = 0; i < storage.size(); i++) {
				RowCol temp = storage.at(i);
				gameMatrix[temp.Row][temp.Col] = currentPlayer;
			}
		}
	}

	storage.clear();
	validflip = true;

	//Check if valid UpRight
	if (gameMatrix[Row + 1][Col + 1] == oppsitePlayer) {
		/*cout << "Check Flip Up_Right" << endl;*/
		int highest_Temp;
		if (Row > Col) {
			highest_Temp = Row;
		}
		else {
			highest_Temp = Col;
		}
		int temp_Row = Row + 1;
		int temp_Col = Col + 1;
		for (int i = highest_Temp + 1; i < tablesizes; i++) {
			if (gameMatrix[temp_Row][temp_Col] == currentPlayer) {
				validflip = true;
				break;
			}
			else if (gameMatrix[temp_Row][temp_Col] == oppsitePlayer) {
				storage.push_back(RowCol(temp_Row, temp_Col));
			}
			else {
				validflip = false;
				break;
			}
			temp_Row++;
			temp_Col++;
		}
		if (validflip) {
			/*cout << "Flip UP_Right" << endl;*/
			for (int i = 0; i < storage.size(); i++) {
				RowCol temp = storage.at(i);
				gameMatrix[temp.Row][temp.Col] = currentPlayer;
			}
		}
	}

	storage.clear();
	validflip = true;

	//Check if valid UpLeft
	if (gameMatrix[Row + 1][Col - 1] == oppsitePlayer) {
		/*cout << "Check Flip Up Left" << endl;*/
		int highest_Temp;
		if (abs(Col - 7) > Row) {
			highest_Temp = abs(Col - 7);
		}
		else {
			highest_Temp = Row;
		}
		int temp_Row = Row + 1;
		int temp_Col = Col - 1;
		/*cout << abs(Col - tablesizes) << ":" << Row << endl;*/
		for (int i = highest_Temp + 1; i < tablesizes; i++) {
			if (gameMatrix[temp_Row][temp_Col] == currentPlayer) {
				validflip = true;
				break;
			}
			else if (gameMatrix[temp_Row][temp_Col] == oppsitePlayer) {
				storage.push_back(RowCol(temp_Row, temp_Col));
			}
			else {
				validflip = false;
				break;
			}
			temp_Row++;
			temp_Col--;
		}
		if (validflip) {
			/*cout << "Flip UP_Left" << endl;*/
			for (int i = 0; i < storage.size(); i++) {
				RowCol temp = storage.at(i);
				gameMatrix[temp.Row][temp.Col] = currentPlayer;
			}
		}
	}

	storage.clear();
	validflip = true;

	//Check if valid BottomRight
	if (gameMatrix[Row - 1][Col + 1] == oppsitePlayer) {
		/*cout << "Bottom Right" << endl;*/
		int highest_Temp;
		if (abs(Row - 7) > Col) {
			highest_Temp = abs(Row - 7);
		}
		else {
			highest_Temp = Col;
		}
		int temp_Row = Row - 1;
		int temp_Col = Col + 1;
		/*cout << abs(Col - tablesizes) << ":" << Row << endl;*/
		for (int i = highest_Temp + 1; i < tablesizes; i++) {
			if (gameMatrix[temp_Row][temp_Col] == currentPlayer) {
				validflip = true;
				break;
			}
			else if (gameMatrix[temp_Row][temp_Col] == oppsitePlayer) {
				storage.push_back(RowCol(temp_Row, temp_Col));
			}
			else {
				validflip = false;
				break;
			}
			temp_Row--;
			temp_Col++;
		}
		if (validflip) {
			/*cout << "Flip UP_Left" << endl;*/
			for (int i = 0; i < storage.size(); i++) {
				RowCol temp = storage.at(i);
				gameMatrix[temp.Row][temp.Col] = currentPlayer;
			}
		}
	}

	storage.clear();
	validflip = true;

	//Check if valid BottomLeft
	if (gameMatrix[Row - 1][Col - 1] == oppsitePlayer) {
		/*cout << "Bottom Left" << endl;*/
		int highest_Temp;
		if (abs(Row - 7) > abs(Col - 7)) {
			highest_Temp = abs(Row - 7);
		}
		else {
			highest_Temp = abs(Col - 7);
		}
		int temp_Row = Row - 1;
		int temp_Col = Col - 1;
		/*cout << abs(Col - tablesizes) << ":" << Row << endl;*/
		for (int i = highest_Temp + 1; i < tablesizes; i++) {
			if (gameMatrix[temp_Row][temp_Col] == currentPlayer) {
				validflip = true;
				break;
			}
			else if (gameMatrix[temp_Row][temp_Col] == oppsitePlayer) {
				storage.push_back(RowCol(temp_Row, temp_Col));
			}
			else {
				validflip = false;
				break;
			}
			temp_Row--;
			temp_Col--;
		}
		if (validflip) {
			/*cout << "Flip UP_Left" << endl;*/
			for (int i = 0; i < storage.size(); i++) {
				RowCol temp = storage.at(i);
				gameMatrix[temp.Row][temp.Col] = currentPlayer;
			}
		}
	}
}
int Game::findPoints(int player)
{
	int returnpoints = 0;
	for (int Row = 0; Row < tablesizes; Row++) {
		for (int Col = 0; Col < tablesizes; Col++) {
			if (gameMatrix[Row][Col] == player) {
				returnpoints++;
			}
		}
	}
	return returnpoints;
}
vector<RowCol> Game::findValidMove()
{
	//Brute Force Checking All Row and Col
	vector<RowCol> returnStorage;
	for (int row = 0; row < tablesizes; row++) {
		for (int col = 0; col < tablesizes; col++) {
			if (checkIfCanMove(row, col)) {
				returnStorage.push_back(RowCol(row, col));
			}
		}
	}
	return returnStorage;
}
