#include "GreedyBot.h"

greedyBot::greedyBot(int player)
{
	this->player = player;
}

RowCol greedyBot::findBestMove(int gameMatrix[tablesizes][tablesizes], vector<RowCol> validMove)
{
	/*cout << "Current Player" << player << endl;*/
	//Should be Dynamic Array but Error Happens
	vector<int> points;
	for (int i = 0; i < validMove.size(); i++) {

		points.push_back(0);
	}
	for (int i = 0; i < validMove.size(); i++) {
		int oppsitePlayer;
		if (player == 1) {
			oppsitePlayer = 2;
		}
		else {
			oppsitePlayer = 1;
		}
		bool isValid = false;
		//Check if valid Top

		RowCol tempRowColVar = validMove.at(i);

		if (gameMatrix[tempRowColVar.Row+1][tempRowColVar.Col] == oppsitePlayer) {
			/*cout << "top" << endl;*/
			int tempPoints = 0;
			for (int temp_Row = tempRowColVar.Row + 1; temp_Row < tablesizes; temp_Row++) {
				if (gameMatrix[temp_Row][tempRowColVar.Col] == player) {
					points.at(i) += tempPoints;
					break;
				}
				else if (gameMatrix[temp_Row][tempRowColVar.Col] == oppsitePlayer) {
					tempPoints++;
				}
				else {
					break;
				}
			}
		}

		//Check if valid Bottom
		 if (gameMatrix[tempRowColVar.Row - 1][tempRowColVar.Col] == oppsitePlayer) {
			/*cout << "bottom" << endl;*/
			int tempPoints = 0;
			for (int temp_Row = tempRowColVar.Row - 1; temp_Row >= 0; temp_Row--) {
				if (gameMatrix[temp_Row][tempRowColVar.Col] == player) {
					points.at(i) += tempPoints;
					break;
				}
				else if (gameMatrix[temp_Row][tempRowColVar.Col] == oppsitePlayer) {
					tempPoints++;
				}
				else {
					break;
				}
			}
		}

		//Check if valid Right
		if (gameMatrix[tempRowColVar.Row][tempRowColVar.Col + 1] == oppsitePlayer) {
		/*	cout << "Right" << endl;*/
			int tempPoints = 0;
			for (int temp_Col = tempRowColVar.Col + 1; temp_Col < tablesizes; temp_Col++) {
				if (gameMatrix[tempRowColVar.Row][temp_Col] == player) {
					points.at(i) += tempPoints;
					break;
				}
				else if (gameMatrix[tempRowColVar.Row][temp_Col] == oppsitePlayer) {
					tempPoints++;
				}
				else {
					break;
				}

			}
		}

		//Check if valid Left
		if (gameMatrix[tempRowColVar.Row][tempRowColVar.Col - 1] == oppsitePlayer) {
			/*cout << "Left" << endl;*/
			int tempPoints = 0;
			for (int temp_Col = tempRowColVar.Col - 1; temp_Col >= 0; temp_Col--) {
				if (gameMatrix[tempRowColVar.Row][temp_Col] == player) {
					points.at(i) += tempPoints;
					break;
				}
				else if (gameMatrix[tempRowColVar.Row][temp_Col] == oppsitePlayer) {
					tempPoints++;
				}
				else {
					break;
				}
			}
		}

		//Check if valid UpRight
		if (gameMatrix[tempRowColVar.Row + 1][tempRowColVar.Col + 1] == oppsitePlayer) {
			/*cout << "Up_Right" << endl;*/
			int tempPoints = 0;
			int highest_Temp;
			if (tempRowColVar.Row > tempRowColVar.Col) {
				highest_Temp = tempRowColVar.Row;
			}
			else {
				highest_Temp = tempRowColVar.Col;
			}
			int temp_Row = tempRowColVar.Row + 1;
			int temp_Col = tempRowColVar.Col + 1;
			for (int temp = highest_Temp + 1; temp < tablesizes; temp++) {
				if (gameMatrix[temp_Row][temp_Col] == player) {
					points.at(i) += tempPoints;
					break;
				}
				else if (gameMatrix[temp_Row][temp_Col] == oppsitePlayer) {
					tempPoints++;
				}
				else {
					break;
				}
				temp_Row++;
				temp_Col++;
			}
		}
		//Check if valid UpLeft
		else if (gameMatrix[tempRowColVar.Row + 1][tempRowColVar.Col - 1] == oppsitePlayer) {
			/*cout << "Up Left" << endl;*/
			int tempPoints = 0;
			int highest_Temp;
			if (abs(tempRowColVar.Col - tablesizes - 1) > tempRowColVar.Row) {
				highest_Temp = abs(tempRowColVar.Col - tablesizes - 1);
			}
			else {
				highest_Temp = tempRowColVar.Row;
			}
			int temp_Row = tempRowColVar.Row + 1;
			int temp_Col = tempRowColVar.Col - 1;
			for (int temp = highest_Temp + 1; temp < tablesizes; temp++) {
				if (gameMatrix[temp_Row][temp_Col] == player) {
					points.at(i) += tempPoints;
					break;
				}
				else if (gameMatrix[temp_Row][temp_Col] == oppsitePlayer) {
					tempPoints++;
				}
				else {
					break;
				}
				temp_Row++;
				temp_Col--;
			}
		}

		//Check if valid BottomRight
		else if (gameMatrix[tempRowColVar.Row - 1][tempRowColVar.Col + 1] == oppsitePlayer) {
			/*cout << "Bottom Right" << endl;*/
			int tempPoints = 0;
			int highest_Temp;
			if (abs(tempRowColVar.Row - tablesizes - 1) > tempRowColVar.Col) {
				highest_Temp = abs(tempRowColVar.Row - tablesizes - 1);
			}
			else {
				highest_Temp = tempRowColVar.Col;
			}
			int temp_Row = tempRowColVar.Row - 1;
			int temp_Col = tempRowColVar.Col + 1;
			/*cout << abs(Col - tablesizes) << ":" << Row << endl;*/
			for (int temp = highest_Temp + 1; temp < tablesizes; temp++) {
				if (gameMatrix[temp_Row][temp_Col] == player) {
					points.at(i)  += tempPoints;
					break;
				}
				else if (gameMatrix[temp_Row][temp_Col] == oppsitePlayer) {
					tempPoints++;
				}
				else {
					break;
				}
				temp_Row--;
				temp_Col++;
			}
		}

		//Check if valid BottomLeft
		else if (gameMatrix[tempRowColVar.Row - 1][tempRowColVar.Col - 1] == oppsitePlayer) {
			/*cout << "Bottom Left" << endl;*/
			int tempPoints = 0;
			int highest_Temp;
			if (abs(tempRowColVar.Row - tablesizes - 1) > abs(tempRowColVar.Col - tablesizes - 1)) {
				highest_Temp = abs(tempRowColVar.Row - tablesizes - 1);
			}
			else {
				highest_Temp = abs(tempRowColVar.Col - tablesizes - 1);
			}
			int temp_Row = tempRowColVar.Row - 1;
			int temp_Col = tempRowColVar.Row - 1;
			/*cout << abs(Col - tablesizes) << ":" << Row << endl;*/
			for (int temp = highest_Temp + 1; temp < tablesizes; temp++) {
				//cout << temp_Row << ":" << temp_Col << endl;
				//cout << gameMatrix[temp_Row][temp_Col] << endl;
				if (gameMatrix[temp_Row][temp_Col] == player) {
					points.at(i) += tempPoints;
					break;
				}
				else if (gameMatrix[temp_Row][temp_Col] == oppsitePlayer) {
					tempPoints++;
				}
				else {
					break;
				}
				temp_Row--;
				temp_Col--;
			}
		}
	}

	int bestPoints = -1;
	RowCol returnRowCol(0,0);
	for (int i = 0; i < points.size(); i++) {
		cout << "Number:" << i;
		cout << ":Row:" << validMove.at(i).Row;
		cout << ":Col:" << validMove.at(i).Col;
		cout << ":Points:" << points[i] << endl;
		if (points[i] > bestPoints) {
			bestPoints = points[i];
			returnRowCol = validMove.at(i);
		}
	}
	return returnRowCol;
}

RowCol greedyBot::botGreedyMove(int gameMatrix[tablesizes][tablesizes], vector<RowCol> validMove)
{
	return findBestMove(gameMatrix,validMove);
}