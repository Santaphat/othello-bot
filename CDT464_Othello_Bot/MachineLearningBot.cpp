#include "MachineLearningBot.h"

MachineLearningBot::MachineLearningBot(int player)
{

	this->player = player;
	/* Seed */
	std::random_device rd;

	/* Random number generator */
	std::default_random_engine generator(rd());

	/* Distribution on which to apply the generator */
	std::uniform_int_distribution<long long unsigned> distribution(0x00000000, 0xFFFFFFFFFFFFFFFF);

	for (int i = 0; i < tablesizes; i++) {
		for (int j = 0; j < tablesizes; j++) {
			long long b = distribution(generator);
			long long w = distribution(generator);
			zobristKeys[i][j].setBW(b, w);
		}
	}
}

RowCol MachineLearningBot::machineLearningBotMove(int gameMatrixs[tablesizes][tablesizes], std::vector<RowCol> validMove)
{
	return findBestWinChance(gameMatrixs, validMove);
}

RowCol MachineLearningBot::findBestWinChance(int gameMatrix[tablesizes][tablesizes], std::vector<RowCol> validMove)
{

	std::vector<int> bestProbofWinVec;
	float bestProbofWin = _I64_MIN;

	//First Value
	RowCol temp = validMove.at(0);
	gameMatrix[temp.Row][temp.Col] = player;
	long long firstUnNumb = getUnquieNumber(gameMatrix);
	float firstPoint = hashData.getHash(firstUnNumb);
	if (firstPoint == NULL) {
		hashData.hashNum(firstUnNumb, 1.f);
		bestProbofWinVec.push_back(0);
		bestProbofWin = 1.f;
	}
	else {
		bestProbofWinVec.push_back(0);
		bestProbofWin = firstPoint;
	}

	//Get All Prob Value
	for (int i = 1; i < validMove.size(); i++) {
		RowCol temp = validMove.at(i);
		gameMatrix[temp.Row][temp.Col] = player;
		long long tempUnNumb = getUnquieNumber(gameMatrix);
		float tempPoint = hashData.getHash(tempUnNumb);
		if (tempPoint == NULL) {
			hashData.hashNum(tempUnNumb, 1.f);
			if (bestProbofWin < 1.f) {
				bestProbofWinVec.clear();
				bestProbofWin = 1.f;
				bestProbofWinVec.push_back(i);
			}
			else if (bestProbofWin == 1.f) {
				bestProbofWinVec.push_back(i);
			}
		}
		else {
			if (bestProbofWin < tempPoint) {
				bestProbofWinVec.clear();
				bestProbofWin = tempPoint;
				bestProbofWinVec.push_back(i);
			}
			else if (bestProbofWin == tempPoint) {
				bestProbofWinVec.push_back(i);

			}
		}
		gameMatrix[temp.Row][temp.Col] = 0;
	}

	//If similar then we random and insert the way the game went so we could alter the winnig prob
	int tempRand = rand() % bestProbofWinVec.size();
	RowCol bestMove = validMove.at(bestProbofWinVec.at(tempRand));
	gameMatrix[bestMove.Row][bestMove.Col] = player;
	long long tempUnNumb = getUnquieNumber(gameMatrix);
	unquieMatrix.push_back(tempUnNumb);
	gameMatrix[bestMove.Row][bestMove.Col] = 0;
	bestProbofWinVec.clear();
	return bestMove;


}

void MachineLearningBot::upDateBot(bool win)
{
	std::cout << "Size:" << unquieMatrix.size() << std::endl;
	for (int i = 0; i < unquieMatrix.size(); i++) {
		if (win) {
			float temp_Data_Inside = hashData.getHash(unquieMatrix.at(i));
			float tempData = hashData.getHash(unquieMatrix.at(i)) + plusWin;
			if (tempData > 10) {
				tempData = 10.f;
			}
			hashData.hashNum(unquieMatrix.at(i), tempData);
			
		}
		else if (!win) {
			float temp_Data_Inside = hashData.getHash(unquieMatrix.at(i));
			float tempData = hashData.getHash(unquieMatrix.at(i)) - minusLose;
			if (tempData < 0) {
				tempData = 0;
			}
			hashData.hashNum(unquieMatrix.at(i), tempData);
		}
	}
	unquieMatrix.clear();
}

long long MachineLearningBot::getUnquieNumber(int gameMatrix[tablesizes][tablesizes])
{
	long long unique_num = 0;
	bool start = false;
	for (int i = 0; i < tablesizes; i++) {
		for (int j = 0; j < tablesizes; j++) {
			int temp = gameMatrix[i][j];
			if (temp != 0) {
				if (temp == 1) {
					unique_num = unique_num ^= zobristKeys[i][j].b;

				}
				else if (temp == 2) {
					unique_num = unique_num ^= zobristKeys[i][j].w;
				}
			}
		}
	}
	return unique_num;
}

void MachineLearningBot::printHashTable()
{
	hashData.printHashTable();
}

