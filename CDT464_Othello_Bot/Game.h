#pragma once
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <vector>
#include <math.h>
#include "GreedyBot.h"
#include "Struct.h"
//Test Delete
#include "MachineLearningBot.h"

//Black = 1;
//White = 2;
using namespace std;
class Game
{
public:
	Game();
	void initGame();
	void resetGame();
	void printTable();
	void gameLoop();
	void playerInput(int Row, int Col);
	bool checkIfCanMove(int Row, int Col);
	void flipCheck(int Row, int Col);
	int findPoints(int player);

	vector<RowCol> findValidMove();
private:

	static const int tablesizes = 6;
	int gameMatrix[tablesizes][tablesizes];
	int blackPlayerWin;
	int whitePlayerWin;
	int blackPlayerPointsCount;
	int whitePlayerPointsCount;
	int currentPlayer;
	bool blackPlayerNoMove;
	bool whitePlayerNoMove;
	greedyBot bot1 = greedyBot(NULL);
	greedyBot bot2 = greedyBot(NULL);
	MachineLearningBot testBot = MachineLearningBot(NULL);
};
