#pragma once
#include <iostream>
#include <vector>

#include "Struct.h"
using namespace std;
class greedyBot
{
private:
	static const int tablesizes = 6;
public:
	greedyBot(int player);
	RowCol botGreedyMove(int gameMatrix[tablesizes][tablesizes], vector<RowCol> validMove);
private:
	
	int player;
	RowCol findBestMove(int gameMatrix[tablesizes][tablesizes], vector<RowCol> validMove);


};